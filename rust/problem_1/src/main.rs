fn sum_of_multiples(n: u32, max: u32) -> u32 {
	let mut sum: u32 = 0;
    let limit = 1 + ((max - 1)/n);

	for i in 1..limit {
        sum += i * n;
	}

	return sum;
}

fn main() {
	let result = sum_of_multiples(3, 1000) + sum_of_multiples(5, 1000) - sum_of_multiples(15, 1000);

    println!("{}", result);
}
