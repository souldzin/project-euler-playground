fn is_integer(val: f64) -> bool {
    return val.fract() == 0.0;
}

fn find_pythagorean_triplet_with_sum(target_sum: i32) -> (i32, i32, i32) {
    // Equation1: a^2 + b^2 = c^2
    // Equation2: a + b + c = TS
    // 
    // Simplifies to: a ^ 2 + b ^ 2 = (TS - a - b) ^ 2
    // Simplifies to: 0 = TS ^ 2 - 2 * TS * a - 2 * TS * b + 2 * a * b
    // Simplifies to: TS * b - a * b = (TS^2) / 2 - (TS * a)
    // Simplifies to: b * (TS - a) = (TS^2) / 2 - (TS * a)
    // Simplifies to: b = (TS * (a - TS/2)) / (a - TS)

    // We are only interested in natural numbers (where a, b, and c are positive integers)
    // This means we are constraining ourselves to a < TS/2 since that will make both
    // (a - TS/2) and (a - TS) negative, causing the whole division to be positive.

    let f_target_sum: f64 = target_sum as f64;
    let half_target_sum = target_sum / 2;
    let f_half_target_sum = f64::from(half_target_sum);

    for a in 1..half_target_sum {
        let f_a = f64::from(a);
        let f_b = (f_target_sum * (f_a - f_half_target_sum)) / (f_a - f_target_sum);

        if is_integer(f_b) {
            return (a, f_b as i32, (f_target_sum - f_a - f_b) as i32)
        }
    }

    // TODO: What to return when no solution found
    (0, 0, 0)
}

fn main() {
    let target_sum_arg = std::env::args().nth(1);

    let target_sum = target_sum_arg
        .expect("Error! 'target_sum_arg' not found and should be the first argument in the command (try 25).")
        .parse::<i32>()
        .expect("Error! 'target_sum_arg' is not an integer.");

    println!("{}", target_sum);

    let result = find_pythagorean_triplet_with_sum(target_sum);
    let product = result.0 * result.1 * result.2;

    println!("The Pythagorean triplet whose sum is {}: {:?}", target_sum, result);
    println!("The product is: {}", product);
}

