use std::collections::HashMap;
// 
// 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
// 
// What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
// 
// 10 = 5 * 2
// 9 = 3 * 3
// 8 = 2 * 2 * 2
// 7
// 6 = 3 * 2
// 5
// 4 = 2 * 2
// 3
// 2
// 
// 2520 = 7 * 5 * 3 * 3 * 2 * 2 * 2
// 
// Algorithm:
// 1) Find sets of prime factors for 1...n
// 2) Find maximum number of occurrences for each prime factor

fn is_factor(a: u32, b: u32) -> bool {
    return b % a == 0;
}

fn find_prime_factors(n: u32) -> Vec<u32> {
    let max = ((n as f32).sqrt().floor() as u32) + 1;

    for i in 2..max {
        if (is_factor(i, n)) {
            let pair = n / i;
            let mut factors = find_prime_factors(pair);
            factors.push(i);
            return factors;
        }
    }

    return vec![n];
}

fn main() {
    let mut result_prime_factors: HashMap<u32, u32> = HashMap::new();

    let factor_sets = (2..21).map(|n| {
        return find_prime_factors(n);
    });

    for factors in factor_sets {
        let mut prime_factor: HashMap<u32, u32> = HashMap::new();

        // Count occurences of factors
        for factor in factors {
            prime_factor.insert(factor, prime_factor.get(&factor).unwrap_or(&0) + 1);
        }

        // Check if we need to update our result
        for factor in prime_factor.keys() {
            let count = prime_factor.get(factor).unwrap();

            if (count > result_prime_factors.get(factor).unwrap_or(&0)) {
                result_prime_factors.insert(*factor, *count);
            }
        }
    }

    let mut result = 1;

    for factor in result_prime_factors.keys() {
        let count = result_prime_factors.get(factor).unwrap();

        for i in (0..*count) {
            result *= *factor;
        }
    }

    println!("{}", result);
}
