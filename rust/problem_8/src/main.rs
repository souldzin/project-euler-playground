
fn find_greatest_adjacent_product(numbers: &[i32], product_size: usize) -> (u64, Vec<i32>) {
    println!("Size of numbers: {}", numbers.len());
    println!("Product size: {}", product_size);

    let numbers_len = numbers.len();

    // TODO: What to do if 0 is in the first set of numbers?
    let mut current_slice = Vec::from(&numbers[..product_size]);
    let mut current_product: u64 = current_slice.iter().fold(1, |acc, x| acc * (*x as u64));
    let mut greatest_slice = current_slice.clone();
    let mut greatest_product = current_product;

    let mut last_i_with_zero = 0;
    let mut greatest_i = product_size - 1;

    for i in product_size..numbers_len {
        let old_idx = i - product_size;
        let new_val = numbers[i];

        // If we've found a new 0
        if new_val == 0 {
            current_product = 0;
            last_i_with_zero = i;
        }
        // If we've left the 0's!
        else if old_idx == last_i_with_zero {
            // Then, let's calculate the new product
            current_product = current_slice[1..].iter().fold(new_val as u64, |acc, x| acc * (*x as u64))
        }
        // If we don't have to worry about 0's
        else if current_product != 0 {
            let old_val = current_slice[0];

            current_product /= old_val as u64;
            current_product *= new_val as u64;
        }

        // Keep track of the history
        current_slice.remove(0);
        current_slice.push(new_val);

        // Update the "greatest"
        if current_product > greatest_product {
            greatest_product = current_product;
            greatest_i = i;
            greatest_slice = current_slice.clone();
        }
    }

    (greatest_product, greatest_slice)
}

fn main() {
    let product_size_arg = std::env::args().nth(1);
    let numbers_arg = std::env::args().nth(2);

    let product_size = product_size_arg
        .expect("Error! 'product_size_arg' not found and should be the first argument in the command.")
        .parse::<usize>()
        .expect("Error! 'product_size_arg' is not an integer.");

    let numbers: Vec<i32> = numbers_arg.expect("Error! 'numbers_arg' not found and should be the second argument in the command.")
        .chars()
        .map(|x| match x {
            '0'..='9' => x.to_string().parse::<i32>().unwrap(),
            _ => -1
        })
        .filter(|&x| x >= 0)
        .collect();

    let (product, numbers) = find_greatest_adjacent_product(&numbers, product_size);

    println!("Largest product in a series: {}", product);
    println!("Series: {:?}", numbers);
}
