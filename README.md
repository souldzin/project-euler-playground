# Project Euler Playground

Just some random solutions to problems on https://projecteuler.net/

## Running solutions in `deno/`

You'll want to install deno.

1. `cd` to the `deno/` directory in the project.
2. Run a specific problem with:

```shell
deno run problem_1.ts
```

## Running solutions in `rust/`

You'll want to install rust.

1. `cd` to the `rust/` directory in the project.
2. `cd` to the specific problem folder
3. Run the problem with:

```shell
cargo run
```