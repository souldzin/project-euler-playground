from math import ceil, sqrt, floor
from typing import List 

last_message = None

def clear_progress():
  global last_message
  if last_message:
    print('\r', end='', flush=True)

def show_progress(tick, total):
  global last_message
  clear_progress()
  
  message = str(ceil(100*tick/total)) + "%..."

  print(message, end='', flush=True)
  last_message = message

def is_prime(candidate: int, known_primes: List[int]) -> bool:
  # Assumes known_primes is sorted
  max_possible_prime = floor(sqrt(candidate))

  for known_prime in known_primes:
    if known_prime > max_possible_prime:
      return True
    elif candidate % known_prime == 0:
      return False
  
  return True


def find_primes(max: int) -> List[int]:
  if max < 2:
    return []
  if max < 3:
    return [2]

  primes = [2,3]
  
  for candidate in range(5,max,2):
    show_progress(candidate, max)
    # noop
    if is_prime(candidate, primes):
      primes.append(candidate)
  
  clear_progress()

  return primes

def find_sum_of_all_primes_below(max: int) -> int:
  primes = find_primes(max)
  return sum(primes)


def main():
  limit = 2000000

  result = find_sum_of_all_primes_below(limit)

  print("Sum of all primes below " + str(limit) + ":  " + str(result))


if __name__ == '__main__':
  main()
