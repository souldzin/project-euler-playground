let ticks = 0;

export function getTicks()
{
    return ticks;
}

export function tick()
{
    ticks++;
}
