function isPrime(
	num: number,
	orderedKnownPrimes: number[]
): boolean {
	const maxPossibleFactor = Math.floor(Math.sqrt(num));

	let maxIdx = orderedKnownPrimes.findIndex(x => x > maxPossibleFactor);
	if (maxIdx === -1)
	{
		maxIdx = orderedKnownPrimes.length;
	}

	for (let i = 0; i < maxIdx; i++)
	{
		const current = orderedKnownPrimes[i];

		if (num % current === 0)
		{
			return false;
		}
	}

	return true;
}


function main() {
	const primeCount = 10001;
	
	const knownPrimes = new Array(primeCount);
	knownPrimes[0] = 2;
	let nextPrimesIdx = 1;
	let current = 3;

	while (nextPrimesIdx < primeCount)
	{
		if (isPrime(current, knownPrimes))
		{
			knownPrimes[nextPrimesIdx] = current;
			nextPrimesIdx++;
		}

		current += 2;	
	}

	console.log(knownPrimes[nextPrimesIdx - 1]);
}

main();