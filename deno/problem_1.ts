/*
If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.

Find the sum of all the multiples of 3 or 5 below 1000.
*/
function sumOfMultiples(n: number, max: number = 1000) {
	let sum = 0;

	for(let i = n; i < max; i += n) {
		sum += i;
	}

	return sum;
}

function main() {
	const result = sumOfMultiples(3) + sumOfMultiples(5) - sumOfMultiples(15);

	console.log(result);
}

main();
