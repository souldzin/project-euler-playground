import { getTicks, tick } from "./utils/tick.ts";

function isPalindrome(
    chars: string
): boolean
{
    const max = Math.floor(chars.length/2);
    for (let startIdx = 0; startIdx < max; startIdx++)
    {
        let startChar = chars[startIdx];
        let endChar = chars[chars.length - startIdx - 1];

        if (startChar !== endChar)
        {
            return false;
        }
    }

    return true;
}

function isPalindromeNumber(
    num: number
)
{
    const chars = String(Math.abs(num));
    return isPalindrome(chars);
}

function findLargestPalindrome(
    min: number,
    max: number
): {
    palindrome: number,
    factors: [number, number] | undefined
}
{
    let result : number = -1;
    let factors: [number, number] | undefined;
    for (let i = max; i > min; i--)
    {
        if (factors && i < factors[1])
        {
            break;
        }

        for (let j = i; j > min; j--)
        {
            tick();

            const mult = i * j;
            if (mult < result)
            {
                break;
            }

            if (isPalindromeNumber(mult))
            {
                result = mult;
                factors = [i, j];
                break;
            }
        }
    }

    return {
        palindrome: result,
        factors
    };
}

function main()
{
    const largestPalindrome = findLargestPalindrome(100, 999);
    console.log(largestPalindrome, getTicks());
}

main();
