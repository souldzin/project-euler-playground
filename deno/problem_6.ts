
function getSumOfSquares(
	max: number
): number {
	let val = 0;
	for (let i = 1; i <= max; i++)
	{
		val += i * i;
	}

	return val;
}

function getSquareOfSums(
	n: number
): number {
	const sum = (n * (n + 1))	/ 2;
	return sum * sum;
}

function main() {
	const max = 100;
	const sumOfSquares = getSumOfSquares(max);
	const squareOfSums = getSquareOfSums(max);
	const diff = squareOfSums - sumOfSquares;

	console.log(`${squareOfSums} - ${sumOfSquares} = ${diff}`);
}

main();

