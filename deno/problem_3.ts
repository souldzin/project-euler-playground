let ticks = 0;

function isFactor(
    a: number,
    b: number
): boolean {
    return b % a === 0;
}

function findLargestPrimeFactor(
    a: number
): number {
    const max = Math.floor(Math.sqrt(a));

    // TODO. I think there's a bug here... We probably want to <= max
    for (let i = 2; i < max; i++)
    {
        ticks++;

        if (isFactor(i, a))
        {
            const factorPair = a / i;
            return findLargestPrimeFactor(factorPair);
        }
    }

    return a;
}

function main(): void {
    const largestPrimeFactor = findLargestPrimeFactor(600851475143);
    console.log(largestPrimeFactor);
    console.log(`found in ${ticks} ticks!`);
}

main();
